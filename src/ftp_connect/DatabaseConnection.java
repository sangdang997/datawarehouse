package ftp_connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DatabaseConnection {
	
	//Kết nối đến Sql Server
	public static Connection getConnection() {
		Connection connection = null;
		String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String url = "jdbc:sqlserver://localhost:1433;";
		String user = "sa";
		String pass = "dts11091997";
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, user, pass);
		} catch (Exception e) {
//			e.printStackTrace();
			System.out.println("Kết nối thất bại");
		}
		return connection;
	}

	// thuc thi lenh sql
	public void perform(String sql) throws Exception {
		Connection connect = getConnection();
		Statement stmt = connect.createStatement();
		stmt.executeUpdate(sql);
		stmt.close();
	}
	
	// chọn dữ liệu sql
	public ResultSet selectDatabase(String sql) throws Exception {
		Connection connect = getConnection();
		Statement stmt = connect.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}

	public static void main(String[] args) {
		Connection connection = DatabaseConnection.getConnection();
		if (connection != null) {
			System.out.println("Connection Successful ");
		} else {
			System.out.println("Connection Failed");
		}
	}

}
