package model;

public class DataMart {
	String date;
	int count;
	public DataMart(String date, int count) {
		super();
		this.date = date;
		this.count = count;
	}
	public String getDate() {
		return date;
	}
	public int getCount() {
		return count;
	}
}
