package model;

import java.time.LocalDateTime;

public class Logs {
	private int id;
	private String num_group, file_name, delimiter;
	LocalDateTime date_download;
	String size, status;

	public Logs(int id, String num_group, String file_name, String delimiter, LocalDateTime date_download, String size,
			String status) {
		super();
		this.id = id;
		this.num_group = num_group;
		this.file_name = file_name;
		this.delimiter = delimiter;
		this.date_download = date_download;
		this.size = size;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public String getNum_group() {
		return num_group;
	}

	public String getFile_name() {
		return file_name;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public LocalDateTime getDate_download() {
		return date_download;
	}

	public String getStatus() {
		return status;
	}

	public String getSize() {
		return size;
	}

	@Override
	public String toString() {
		return "Logs [id=" + id + ", num_group=" + num_group + ", file_name=" + file_name + ", delimiter=" + delimiter
				+ ", date_download=" + date_download + ", size=" + size + ", status=" + status + "]";
	}

}
