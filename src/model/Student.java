package model;

import java.time.LocalDate;

public class Student {
	String mssv, ho_ten, ngay_sinh, gioi_tinh, dia_chi;
	LocalDate datedownload;
	String status;

	public Student(String mssv, String ho_ten, String ngay_sinh, String gioi_tinh, String dia_chi, LocalDate datedownload,
			String status) {
		super();
		this.mssv = mssv;
		this.ho_ten = ho_ten;
		this.ngay_sinh = ngay_sinh;
		this.gioi_tinh = gioi_tinh;
		this.dia_chi = dia_chi;
		this.datedownload = datedownload;
		this.status = status;
	}

	public String getMssv() {
		return mssv;
	}

	public String getHo_ten() {
		return ho_ten;
	}

	public String getNgay_sinh() {
		return ngay_sinh;
	}

	public String getGioi_tinh() {
		return gioi_tinh;
	}

	public String getDia_chi() {
		return dia_chi;
	}

	public LocalDate getDatedownload() {
		return datedownload;
	}

	public String getStatus() {
		return status;
	}

}
