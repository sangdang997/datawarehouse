package ftp_download;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

public class FTPFileDowload {
	FTPClient ftp = null;

	public FTPFileDowload(String host, String user, String pwd) throws Exception {
		ftp = new FTPClient();
		// ftp.addProtocolCommandListener(new PrintCommandListener(new
		// PrintWriter(System.out)));
		int reply;
		ftp.connect(host);
		reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftp.disconnect();
			throw new Exception("Exception in connecting to FTP Server");
		}
		ftp.login(user, pwd);
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		ftp.enterLocalPassiveMode();
	}

	public void downloadFile(String remoteFilePath, String localFilePath) {
		try (FileOutputStream fos = new FileOutputStream(localFilePath)) {
			this.ftp.retrieveFile(remoteFilePath, fos);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void disconnect() {
		if (this.ftp.isConnected()) {
			try {
				this.ftp.logout();
				this.ftp.disconnect();
			} catch (IOException f) {
				// do nothing as file is already downloaded from FTP server
			}
		}
	}

	public static void main(String[] args) {
		String host = "waws-prod-sg1-031.ftp.azurewebsites.windows.net";
		String user = "dataWarehouseG3\\dataWarehouseG03";
		String passw = "dataWarehouseG03";

		String fileName = "Data.csv";
		String destFile = "F:\\Data Warehouse\\FTP\\Nhom4.csv";
		try {
			FTPFileDowload ftpDownloader = new FTPFileDowload(host, user, passw);
			ftpDownloader.downloadFile(fileName, destFile);
			System.out.println("FTP File downloaded successfully");
			ftpDownloader.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
