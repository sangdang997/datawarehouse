package ftp_download;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import DAO.StudentDAO;
import ftp_connect.DatabaseConnection;
import model.Student;

public class ImportWarehouse {

	public void ImportDbWarehouse() {
		try {
			// đặt các trường dữ liệu mặc định giống db Datawarehouse
			String group = "";
			String status = "";
			String kq = "";
			String mssv = "mssv";
			String ho = "ho_ten";
			String ho_lot = "ho_lot";
			String ten = "ten";
			String gioitinh = "gioi_tinh";
			String ngaysinh = "ngay_sinh";
			String diachi = "dia_chi";
			// 1. chọn tên nhóm, trạng thái từ data_file_logs
			ResultSet rs = new DatabaseConnection()
					.selectDatabase("select num_group, status from Control.dbo.data_file_logs");
			while (rs.next()) {
				status = rs.getString(2);
				// 2. kiểm tra trạng thái
				if (status.equals("Load Staging Success")) {
					// 3. lấy thông tin các trường của table Staging
					group = rs.getString(1);
					ResultSet rs1 = new DatabaseConnection().selectDatabase(
							"select COLUMN_NAME from Staging.INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '" + group
									+ "' ");
					// tạo list lưu tên các trường
					List<String> arr = new ArrayList<>();
					while (rs1.next()) {
						kq = rs1.getString(1);
						arr.add(kq);
					}
					String P_Mssv = "";
					String P_Ho = "";
					String P_Ten = "";
					String P_GioiTinh = "";
					String P_NgaySinh = "";
					String P_DiaChi = "";
					// lấy dữ liệu các column của table trong Staging
					ResultSet rs2 = new DatabaseConnection().selectDatabase("select * from Staging.dbo." + group + " ");
					while (rs2.next()) {
						// 4. so sánh nếu tên trường table Staging đúng với tên
						// trường trong dataWarehouse thì lấy dữ liệu từng
						// column của
						// table trong Staging truyền vào dataWarehouse
						for (int i = 0; i < arr.size(); i++) {
							if (arr.get(i).equals(mssv)) {
								P_Mssv = rs2.getString(i + 1);
							} else if (arr.get(i).equals(ho) || arr.get(i).equals(ho_lot)) {
								P_Ho = rs2.getString(i + 1);
							} else if (arr.get(i).equals(ten)) {
								P_Ten = rs2.getString(i + 1);
							} else if (arr.get(i).equals(gioitinh)) {
								P_GioiTinh = rs2.getString(i + 1);
							} else if (arr.get(i).equals(ngaysinh)) {
								P_NgaySinh = rs2.getString(i + 1);
							} else if (arr.get(i).equals(diachi)) {
								P_DiaChi = rs2.getString(i + 1);
							}
						}
						// 5. tạo ngày
						LocalDate date = LocalDate.now();

						// 6. tạo status chưa kiểm tra
						String statusWH = "Unchecked";
						// tạo model DataWarehouse
						Student p1 = new Student(P_Mssv, P_Ho + P_Ten, P_NgaySinh, P_GioiTinh, P_DiaChi, date,
								statusWH);
						// 7. thêm dữ liệu vào dataWarehouse
						new StudentDAO().add(p1);
						// 8. Update lại status load DataWarehouse thành công
						String statusLogs = "Load DataWarehouse Success";
						new DatabaseConnection().perform("Update Control.dbo.data_file_logs set status ='" + statusLogs
								+ "' where status ='" + status + "' ");
					}
				}
			}

			System.out.println("Import DataWarehouse Success");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new ImportWarehouse().ImportDbWarehouse();
	}
}
