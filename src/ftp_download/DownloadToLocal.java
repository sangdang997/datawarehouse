package ftp_download;

import java.io.File;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;

import DAO.LogsDAO;
import ftp_connect.DatabaseConnection;
import model.Logs;

public class DownloadToLocal {
	public void DownloadToLocal() {
		try {
			// 1. kết nối db Control
			// 2 . chọn dữ liệu từ table data_file_control
			ResultSet rs = new DatabaseConnection().selectDatabase("select * from Control.dbo.data_file_config");
			while (rs.next()) {
				// lấy trạng thái file của từng host
				String status = rs.getString(9);
				// lấy dữ liệu cần để download
				String filedown = rs.getString(7);
				String csvFile = "F:\\Data Warehouse\\FTP\\" + filedown;
				String host = rs.getString(2);
				String user = rs.getString(3);
				String passw = rs.getString(4);
				String fileName = rs.getString(6);
				String group = rs.getString(5);
				String delimiter = rs.getString(8);

				// tạo thời gian
				LocalDateTime dateDown = LocalDateTime.now(); 

				String size = "";
				String statusLogs = "";

				//3. kiểm tra trạng thái
				if (status.equals("None") || status.equals("Load Local Failed")) {

					// download thành công
					try {
						// 4. download
						FTPFileDowload ftpDownloader = new FTPFileDowload(host, user, passw);
						ftpDownloader.downloadFile(fileName, csvFile);

						// 5. tính dung lượng
						File sizeFile = new File(csvFile);
						double len = sizeFile.length();
						size = len + "\t" + "bytes";
						// tính size của file. nếu 0kb(file rỗng) thì cập nhật
						// trạng thái thất bại
						// nếu có dung lượng thì cập nhật trạng thái thành công
						if (len == 0.0) {
							//6.1 trạng thái thất bại
							statusLogs = "Load Local Failed";
							// 7.1 delete file rỗng trên local
							 File file = new File(csvFile);
							 file.delete();
							System.out.println(group + "\t" + "Failed");
						} else {
							//6.2 trạng thái thành công
							statusLogs = "Load Local Success";
							System.out.println(group + "\t" + "Success");
						}

						// ngắt kết nối download
						ftpDownloader.disconnect();
					}
					// 4.  download thất bại
					catch (Exception e) {
						// set size bằng null. vì down k thành công
						size = null;
						// 6.3  trạng thái thất bại
						statusLogs = "Load Local Failed";
						System.out.println(group + "\t" + "Failed");
					}
					// lấy id trong data_file_logs
					ResultSet rs2;
					rs2 = new DatabaseConnection().selectDatabase("SELECT * FROM Control.dbo.data_file_logs");
					// tạo id trong data_file_logs
					int count = 0;
					while (rs2.next()) {
						int id = rs2.getInt(1);
						count = id + 1;
					}
					// tạo model logs
					Logs logs = new Logs(count, group, filedown, delimiter, dateDown, size, statusLogs);
					// 7.2 thêm vào data_file_logs
					new LogsDAO().add(logs);
					//8. update status trong data_file_config
					new DatabaseConnection().perform("update Control.dbo.data_file_config set status = '" + statusLogs
							+ "' where table_name= '" + group + "' ");
				}
			}
			System.out.println("Download to Local.!!!");
		} catch (Exception e) {
			// báo lỗi
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new DownloadToLocal().DownloadToLocal();
	}
}
