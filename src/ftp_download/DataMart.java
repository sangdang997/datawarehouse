package ftp_download;

import java.sql.ResultSet;
import DAO.MartDAO;
import ftp_connect.DatabaseConnection;

public class DataMart {

	public void DataMart() {
		try {
			String date_download = "";
			int count = 0;
			String status = "Unchecked";
			// 1. kết nối với dataMart
			// 2. kiem tra ton tai bang aggregated neu khong ton tai thi tao bang
			new DatabaseConnection().perform(
					"IF NOT EXISTS(SELECT * FROM DataMart.INFORMATION_SCHEMA.TABLES Where Table_Schema = 'dbo'  AND Table_Name ='Aggregated') BEGIN CREATE TABLE DataMart.dbo.Aggregated(date_down date not null, NumOfStu int not null) END");
			//3. tìm trong datawarehouse dữ liệu chưa được kiểm tra
			//4.  đếm các dữ liệu trùng nhau
			ResultSet rs = new DatabaseConnection().selectDatabase(
					"SELECT Date_download , COUNT(*) AS Count FROM DataWarehouse.dbo.Student Where Status = '" + status
							+ "' GROUP BY Date_download HAVING COUNT(Date_download) >= 1");
			while (rs.next()) {
				// 5. xuất ra các dữ liệu
				date_download = rs.getString(1);
				count = rs.getInt(2);
				// tạo model datamart
				model.DataMart mart = new model.DataMart(date_download, count);
				// 6. thêm dữ liệu vào datamart
				new MartDAO().add(mart);
				// 7. update lại status đã kiểm tra trong datawarehouse
				String statusWh = "Checked";
				new DatabaseConnection().perform("Update DataWarehouse.dbo.Student set Status = '" + statusWh
						+ "' where Status='" + status + "' ");
			}

			// 8. nếu ngày thêm vào giống với ngày thêm trước đó thì transform lại dữ liệu
			//chọn ngày thêm giống nhau trong dataMart tính tổng lại số sv
			ResultSet rsMart = new DatabaseConnection().selectDatabase(
					"SELECT date_down, COUNT(NumOfStu) AS datedown_count, SUM(NumOfStu) AS Sum FROM Datamart.dbo.Aggregated GROUP BY date_down HAVING COUNT(Date_down) > 1");
			while (rsMart.next()) {
				//9. lấy dữ liệu
				String datedown_trans = rsMart.getString(1);
				int count_trans = rsMart.getInt(3);
				//10. thực hiện lệnh xóa dữ liệu bị trùng cũ đi
				new DatabaseConnection()
						.perform("delete DataMart.dbo.Aggregated where date_down='" + datedown_trans + "' ");
				//tạo model mới
				model.DataMart mart_trans = new model.DataMart(datedown_trans, count_trans);
				//11. thêm model mới vào dataMart
				new MartDAO().add(mart_trans);
			}
			System.out.println("Data Mart Success");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new DataMart().DataMart();
	}
}
