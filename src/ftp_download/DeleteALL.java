package ftp_download;

import java.io.File;
import java.sql.ResultSet;

import ftp_connect.DatabaseConnection;

public class DeleteALL {
	public static void main(String[] args) throws Exception {
		new DatabaseConnection().perform("Delete from Control.dbo.data_file_logs");
		new DatabaseConnection().perform("update Control.dbo.data_file_config set status = 'None' ");
		new DatabaseConnection().perform("Delete from DataWarehouse.dbo.Student");
//		new DatabaseConnection().perform("DROP TABLE DataMart.dbo.Aggregated");
		new DatabaseConnection().perform("delete DataMart.dbo.Aggregated");
		ResultSet rs;
		try {
			rs = new DatabaseConnection().selectDatabase("select * from Control.dbo.data_file_config");
			while (rs.next()) {
				String group = rs.getString(5);
				String file_name = rs.getString(7);
				String table = "Staging.dbo." + group;
				// detete Staging
				new DatabaseConnection().perform("delete from " + table + " ");
				// delete file local
				String csvFile = "F:\\Data Warehouse\\FTP\\" + file_name;
				File file = new File(csvFile);
				file.delete();
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		System.out.println("Delete All");
	}
}
