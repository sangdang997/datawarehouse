package ftp_download;

import java.sql.SQLException;

public class Run {
	public static void main(String[] args) throws SQLException {
		System.out.println("Connecting...");
		System.out.println("Downloading...");
		new DownloadToLocal().DownloadToLocal();
		new SaveStaging().SaveDbStaging();
		new ImportWarehouse().ImportDbWarehouse();
		new DataMart().DataMart();
		System.out.println("DONE...!!!");
	}
}
