package ftp_download;

import java.sql.ResultSet;

import ftp_connect.DatabaseConnection;

public class SaveStaging {
	public void SaveDbStaging() {
		ResultSet rs;
		try {
			//1. chọn dữ liệu từ table data_file_logs
			rs = new DatabaseConnection().selectDatabase("select * from Control.dbo.data_file_logs");
			while (rs.next()) {
				//  lấy dữ liệu
				String status = rs.getString(7);
				String num_group = rs.getString(2);
				String delimiter = rs.getString(4);
				//2. File đã download về local status là success.
				// Đọc file đã download về local
				if (status.equals("Load Local Success")) {
					//3. lấy thông file cần đọc
					//lấy tên table trong database Staging
					String table = "Staging.dbo." + num_group;
					//tên file đọc
					String file_name = rs.getString(3);
					// đường dẫn lưu file tại local
					String csvFile = "F:\\Data Warehouse\\FTP\\" + file_name;
					//5.  thực hiện câu lệnh bulk insert đọc toàn bộ file csv vào
					// table của db Staging
					// firstrow: đọc từ dòng thứ 2(bỏ trường).
					// Fieldterminator: delimiter của file.
					// rowterminator: kết thúc hàng bằng xuống dòng /n
					// datafiletype: lưu file dạng unicode (Widechar)
					// tablock: ngăn các nguồn khác đọc dữ liệu đồng thời.
					new DatabaseConnection().perform(
							"BULK INSERT " + table + " from '" + csvFile + "' with(FIRSTROW = 2,  FIELDTERMINATOR = '"
									+ delimiter + "',ROWTERMINATOR = '\n',DATAFILETYPE= 'WIDECHAR',TABLOCK) ");
					// 6. Update lại status load Staging thành công
					String statusLogs = "Load Staging Success";
					new DatabaseConnection().perform("Update Control.dbo.data_file_logs set status ='" + statusLogs
							+ "' where status='" + status + "' ");
				}
			}
			System.out.println("Save Staging...!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new SaveStaging().SaveDbStaging();
	}
}
